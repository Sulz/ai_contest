# myTeam.py
# ---------
# Licensing Information:  You are free to use or extend these projects for
# educational purposes provided that (1) you do not distribute or publish
# solutions, (2) you retain this notice, and (3) you provide clear
# attribution to UC Berkeley, including a link to http://ai.berkeley.edu.
#
# Attribution Information: The Pacman AI projects were developed at UC Berkeley.
# The core projects and autograders were primarily created by John DeNero
# (denero@cs.berkeley.edu) and Dan Klein (klein@cs.berkeley.edu).
# Student side autograding was added by Brad Miller, Nick Hay, and
# Pieter Abbeel (pabbeel@cs.berkeley.edu).


from captureAgents import CaptureAgent
import random, time, util
from game import Directions
import game

#################
# Team creation #
#################

def createTeam(firstIndex, secondIndex, isRed,
               first = 'DummyAgent', second = 'DummyAgent'):
  """
  This function should return a list of two agents that will form the
  team, initialized using firstIndex and secondIndex as their agent
  index numbers.  isRed is True if the red team is being created, and
  will be False if the blue team is being created.

  As a potentially helpful development aid, this function can take
  additional string-valued keyword arguments ("first" and "second" are
  such arguments in the case of this function), which will come from
  the --redOpts and --blueOpts command-line arguments to capture.py.
  For the nightly contest, however, your team will be created without
  any extra arguments, so you should make sure that the default
  behavior is what you want for the nightly contest.
  """

  # The following line is an example only; feel free to change it.
  return [eval(first)(firstIndex), eval(second)(secondIndex)]

##########
# Agents #
##########

class DummyAgent(CaptureAgent):
  """
  A Dummy agent to serve as an example of the necessary agent structure.
  You should look at baselineTeam.py for more details about how to
  create an agent as this is the bare minimum.
  """

  def registerInitialState(self, gameState):
    """
    This method handles the initial setup of the
    agent to populate useful fields (such as what team
    we're on).

    A distanceCalculator instance caches the maze distances
    between each pair of positions, so your agents can use:
    self.distancer.getDistance(p1, p2)

    IMPORTANT: This method may run for at most 15 seconds.
    """

    '''
    Make sure you do not delete the following line. If you would like to
    use Manhattan distances instead of maze distances in order to save
    on initialization time, please take a look at
    CaptureAgent.registerInitialState in captureAgents.py.
    '''
    CaptureAgent.registerInitialState(self, gameState)

    '''
    Your initialization code goes here, if you need any.
    '''
    # Look at the world.
    # self.num_starting_food = 0

    # self.set_team()
    # print '\n\nhere4\n\n'
    # print 'returning random'
    # print random.choice(gameState.getLegalActions(self.index))
    # return random.choice(gameState.getLegalActions(self.index))
    self.alpha = 0.4
    self.discount = 0.6

    self.features = util.Counter()
    self.values = util.Counter()

    # print '\n\nself.chooseAction(gameState)', self.chooseAction(gameState)

    return self.chooseAction(gameState)


  def chooseAction(self, gameState):
        # print 'here too'

        # print 'chooseAction returning random'
        # return random.choice(gameState.getLegalActions(self.index))

        actions = gameState.getLegalActions(self.index)

        current_position = gameState.getAgentPosition(self.index)

        for action in actions:
            successor = gameState.generateSuccessor(self.index, action)
            nxt_state = successor.getAgentPosition(self.index)

            self.update(current_position, action, nxt_state, 1, gameState)


        an_action = self.getAction(nxt_state, gameState)
        print '\n\nfinal_action', an_action

        return self.getAction(nxt_state, gameState)




  def getQValue(self, state, action):
        """
          Returns Q(state,action)
          Should return 0.0 if we have never seen a state
          or the Q node value otherwise
        """
        "*** YOUR CODE HERE ***"

        # print '\n\nhere3\n\n'
        return self.values[(state, action)]

  def computeValueFromQValues(self, state, gameState):
        """
          Returns max_action Q(state,action)
          where the max is over legal actions.  Note that if
          there are no legal actions, which is the case at the
          terminal state, you should return a value of 0.0.
        """
        "*** YOUR CODE HERE ***"

        if not self.getLegalActions(self.index):
            return 0.0

        best = -float('inf')
        # Determining best value from each action to assign to current state.
        for actions in gameState.getLegalActions(self.index):
            # Value of action 'actions' taken from current state
            val = self.getQValue(state, actions)

            if val > best:
                # best value associated with best move from current state
                best = val

        # print '\n\nhere2\n\n'
        return best

  def computeActionFromQValues(self, state, gameState):
        """
          Compute the best action to take in a state.  Note that if there
          are no legal actions, which is the case at the terminal state,
          you should return None.
        """
        "*** YOUR CODE HERE ***"
        # print '\n\nhere1\n\n'
        possible_actions = gameState.getLegalActions(self.index)

        if not possible_actions:
            return None

        best = -float('inf')
        # Finding optimal action
        for actions in possible_actions:
            tmp = self.getQValue(state, actions)
            action = actions

            if action == "Stop":
                pass
            else:

                # if true, 'actions' is better than 'best_action'
                if tmp > best:
                    best = tmp
                    best_action = action

        print 'best_action', best_action
        return best_action

  def getAction(self, state, gameState):
        """
          Compute the action to take in the current state.  With
          probability self.epsilon, we should take a random action and
          take the best policy action otherwise.  Note that if there are
          no legal actions, which is the case at the terminal state, you
          should choose None as the action.

          HINT: You might want to use util.flipCoin(prob)
          HINT: To pick randomly from a list, use random.choice(list)
        """
        # Pick Action
        # print '\n\nhere6\n\n'

        legalActions = gameState.getLegalActions(self.index)
        action = None
        "*** YOUR CODE HERE ***"

        # if util.flipCoin(self.epsilon):
        #     return random.choice(legalActions)

        print '\naction returning:', self.computeActionFromQValues(state, gameState)
        return self.computeActionFromQValues(state, gameState)

  def update(self, state, action, nextState, reward, gameState):
        """
          The parent class calls this to observe a
          state = action => nextState and reward transition.
          You should do your Q-Value update here

          NOTE: You should never call this function,
          it will be called on your behalf
        """
        "*** YOUR CODE HERE ***"
        # print '\n\nhere7\n\n'

        nextState = gameState.generateSuccessor(self.index, action)

        maxQ = -float('inf')
        # getting maxQ(a', s')
        for nxt_action in gameState.getLegalActions(self.index):
            nxt_val = self.getQValue(nextState, nxt_action)
            if nxt_val > maxQ:
                maxQ = nxt_val

        # Checking if terminal
        if maxQ == -float('inf'):
            maxQ = 0

        self.values[(state, action)] = self.getQValue(state, action) + \
                                                self.alpha * (reward + \
                                                self.discount * maxQ - \
                                                self.getQValue(state, action))
        print 'action', action
        print 'self.values[(state, action)]', self.values[(state, action)]
