# AI Contest
Code written for a pacman contest in the AI course.

## To Play
python capture.py -r baselineTeam -b myTeam 

You can replace either baselineTeam or myTeam to adjust the participating agents.
The myTeam found in submit/5pm submit/ was my final submission.

If you are using windows bash, you need to set up display forwarding:

 - Install XMing

 - Run: EXPORT DISPLAY=:0.0


