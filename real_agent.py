# myTeam.py
# ---------
# Licensing Information:  You are free to use or extend these projects for
# educational purposes provided that (1) you do not distribute or publish
# solutions, (2) you retain this notice, and (3) you provide clear
# attribution to UC Berkeley, including a link to http://ai.berkeley.edu.
#
# Attribution Information: The Pacman AI projects were developed at UC Berkeley.
# The core projects and autograders were primarily created by John DeNero
# (denero@cs.berkeley.edu) and Dan Klein (klein@cs.berkeley.edu).
# Student side autograding was added by Brad Miller, Nick Hay, and
# Pieter Abbeel (pabbeel@cs.berkeley.edu).


from captureAgents import CaptureAgent
import random, time, util
from game import Directions
import game

#################
# Team creation #
#################

def createTeam(firstIndex, secondIndex, isRed,
               first = 'DummyAgent', second = 'DummyAgent'):
  """
  This function should return a list of two agents that will form the
  team, initialized using firstIndex and secondIndex as their agent
  index numbers.  isRed is True if the red team is being created, and
  will be False if the blue team is being created.

  As a potentially helpful development aid, this function can take
  additional string-valued keyword arguments ("first" and "second" are
  such arguments in the case of this function), which will come from
  the --redOpts and --blueOpts command-line arguments to capture.py.
  For the nightly contest, however, your team will be created without
  any extra arguments, so you should make sure that the default
  behavior is what you want for the nightly contest.
  """

  # The following line is an example only; feel free to change it.
  return [eval(first)(firstIndex), eval(second)(secondIndex)]

##########
# Agents #
##########

class DummyAgent(CaptureAgent):
  """
  A Dummy agent to serve as an example of the necessary agent structure.
  You should look at baselineTeam.py for more details about how to
  create an agent as this is the bare minimum.
  """

  def registerInitialState(self, gameState):
    """
    This method handles the initial setup of the
    agent to populate useful fields (such as what team
    we're on).

    A distanceCalculator instance caches the maze distances
    between each pair of positions, so your agents can use:
    self.distancer.getDistance(p1, p2)

    IMPORTANT: This method may run for at most 15 seconds.
    """

    '''
    Make sure you do not delete the following line. If you would like to
    use Manhattan distances instead of maze distances in order to save
    on initialization time, please take a look at
    CaptureAgent.registerInitialState in captureAgents.py.
    '''
    CaptureAgent.registerInitialState(self, gameState)

    '''
    Your initialization code goes here, if you need any.
    '''

    # Set our team.
    self.setTeam()


    # Get initial food
    self.num_starting_food = len(self.getFood(gameState).asList())

    # Counter of how many points have been scored.
    self.scored_points = 0

    # Initialize the features dictionary.
    self.features = util.Counter()

    # Find middle of game board.
    self.x_mid_board = len(self.getFood(gameState)[0])
    # self.y_mid_board = i
    # print 'self.y_mid_board', self.y_mid_board

    # Decide what should be the goal based on world state, ie, weights.


  def setTeam(self):
      if self.index % 2 == 0 or not self.index:
          self.team = "Red"
      else:
          self.team = "Blue"

  def chooseAction(self, gameState):
    """
    Picks among actions randomly.
    """
    actions = gameState.getLegalActions(self.index)

    '''
    You should change this in your own agent.
    '''

    # Give each state a weight.
    self.state_vals = util.Counter()

    # Observe the world.
    #   Enemy positions
    #   Food positions
    #   Power pellets
    #
    # Offensive Agent
    #   Search for nearest food.
    #   Search for nearest foe
    #

    # Defensive Agent
    #






    # Select from nearby states the goal.

    # for action in actions:



    return random.choice(actions)


  def look_for_foe(self, gameState):
      foes = []
      curr_pos = gameState.getAgentPosition(self.index)

      for indice in self.getOpponents(gameState):
          if gameState.getAgentPosition(indice):
              foe_pos = gameState.getAgentPosition(indice)
              if self.distancer.getDistance(curr_pos, foe_pos) > 5:
                  if (self.index < 1 and self.foeOnFriendlySideOfBoard(gameState, foe_pos)):
                      foes.append(foe_pos)

              else:
                  foes.append(foe_pos)

      return foes


  def foeOnFriendlySideOfBoard(self, gameState, foe_pos):
      if gameState.isRed(foe_pos) and self.team == "Red":
          return True
      elif not gameState.isRed(foe_pos) and self.team == "Blue":
          return True

      return False


  def fleeOrIgnore(self, agent_index):



  def determinePathWeights(self, goals):



  def getWeights(self):
      return {

        'agent_on_offence' : 100,
        'agent_on_defence' : 100,

        'is_prev_state' : 50,

        'held_food' : 200,
        'nearest_foe_defender' : 600,
        'nearest_target_food' : 500,

      }
