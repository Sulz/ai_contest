# myTeam.py
# ---------
# Licensing Information:  You are free to use or extend these projects for
# educational purposes provided that (1) you do not distribute or publish
# solutions, (2) you retain this notice, and (3) you provide clear
# attribution to UC Berkeley, including a link to http://ai.berkeley.edu.
#
# Attribution Information: The Pacman AI projects were developed at UC Berkeley.
# The core projects and autograders were primarily created by John DeNero
# (denero@cs.berkeley.edu) and Dan Klein (klein@cs.berkeley.edu).
# Student side autograding was added by Brad Miller, Nick Hay, and
# Pieter Abbeel (pabbeel@cs.berkeley.edu).


from captureAgents import CaptureAgent
import random, time, util
from game import Directions
import game

#################
# Team creation #
#################

def createTeam(firstIndex, secondIndex, isRed,
               first = 'DummyAgent', second = 'DummyAgent'):
  """
  This function should return a list of two agents that will form the
  team, initialized using firstIndex and secondIndex as their agent
  index numbers.  isRed is True if the red team is being created, and
  will be False if the blue team is being created.

  As a potentially helpful development aid, this function can take
  additional string-valued keyword arguments ("first" and "second" are
  such arguments in the case of this function), which will come from
  the --redOpts and --blueOpts command-line arguments to capture.py.
  For the nightly contest, however, your team will be created without
  any extra arguments, so you should make sure that the default
  behavior is what you want for the nightly contest.
  """

  # The following line is an example only; feel free to change it.
  return [eval(first)(firstIndex), eval(second)(secondIndex)]

##########
# Agents #
##########

class DummyAgent(CaptureAgent):
  """
  A Dummy agent to serve as an example of the necessary agent structure.
  You should look at baselineTeam.py for more details about how to
  create an agent as this is the bare minimum.
  """

  def registerInitialState(self, gameState):
    """
    This method handles the initial setup of the
    agent to populate useful fields (such as what team
    we're on).

    A distanceCalculator instance caches the maze distances
    between each pair of positions, so your agents can use:
    self.distancer.getDistance(p1, p2)

    IMPORTANT: This method may run for at most 15 seconds.
    """

    '''
    Make sure you do not delete the following line. If you would like to
    use Manhattan distances instead of maze distances in order to save
    on initialization time, please take a look at
    CaptureAgent.registerInitialState in captureAgents.py.
    '''
    CaptureAgent.registerInitialState(self, gameState)

    '''
    Your initialization code goes here, if you need any.
    '''

    def get_target_food_coords2(self, gameState):
        curr_pos = gameState.getAgentPosition(self.index)
        target_food = []
        if self.index < 2:
            for i, j in enumerate(self.getFoodYouAreDefending(gameState)):
                for k, l in enumerate(j):
                    if l:
                        target_food.append((i, k))
        else:
            for i, j in enumerate(self.getFood(gameState)):
                for k, l in enumerate(j):
                    if l:
                        target_food.append((i, k))
        return target_food

    self.starting_target_food = len(get_target_food_coords2(self, gameState))

    self.food_deposited_count = 0

    self.set_team()
    self.features = util.Counter()


  def chooseAction(self, gameState):
    """
    Picks among actions randomly.
    """
    actions = gameState.getLegalActions(self.index)

    '''
    You should change this in your own agent.
    '''

    print '\n\nReturning:', self.rec_expectimax(gameState, gameState.getAgentPosition(self.index), 1, self.index, \
                                (len(self.getTeam(gameState)) + len(self.getOpponents(gameState))))

    # return self.rec_expectimax(gameState, gameState.getAgentPosition(self.index), 1, self.index, \
                                # (len(self.getTeam(gameState)) + len(self.getOpponents(gameState))))[0]
    return random.choice(actions)



  def rec_expectimax(self, gameState, curr_state, depth, agent_num, num_agents):
    lots = float('inf')
    if depth == 0:
        return [self.evaluationFunction(gameState), None]

    # if agent_num == num_agents:
    #     nxt_agent = 0
    # else:
    #     nxt_agent = agent_num + 1

    if self.index in self.getTeam(gameState): # Our turn
        best_value = -lots
        best_move = None
        for moves in gameState.getLegalActions(self.index):
            suc_game_state = gameState.generateSuccessor(self.index, moves)
            child_val = self.rec_expectimax(gameState, suc_game_state, depth - 1, \
                                        nxt_agent, num_agents)

            if best_value < child_val[0]:
                best_value = child_val[0]
                best_move = moves

                if child_val[1] == None:
                    child_val[1] = best_move

        return [best_value, best_move]

    else:
        best_value = 0
        best_move = None
        count = 0
        for moves in gameState.getLegalActions(self.index):
            suc_game_state = gameState.generateSuccessor(self.index, moves)

            """Decrement depth when the next player is pacman."""

            best_value = best_value + \
                            self.rec_expectimax(gameState, suc_game_state, depth - 1, \
                                                    nxt_agent, num_agents)[0]
            count += 1
            best_move = moves

        best_value = float(best_value/count)
        return [best_value, best_move]

  def evaluationFunction(self, gameState):
    """
    This method handles the initial setup of the
    agent to populate useful fields (such as what team
    we're on).

    A distanceCalculator instance caches the maze distances
    between each pair of positions, so your agents can use:
    self.distancer.getDistance(p1, p2)

    IMPORTANT: This method may run for at most 15 seconds.
    """

    '''
    Make sure you do not delete the following line. If you would like to
    use Manhattan distances instead of maze distances in order to save
    on initialization time, please take a look at
    CaptureAgent.registerInitialState in captureAgents.py.
    '''
    # CaptureAgent.registerInitialState(self, gameState)

    '''
    Your initialization code goes here, if you need any.
    '''

    # This is purely for the defensive agent to know the location of the closest defended food to the border
    # Sit near border
    # target_food_coords = self.get_target_food_coords(gameState)
    # furthest = -float('inf')
    # for each in target_food_coords:
    #     if self.distancer.getDistance(gameState.getAgentPosition(self.index), each) > furthest:
    #         # furthest_coord = each
    #         furthest = self.distancer.getDistance(gameState.getAgentPosition(self.index), each)
    #         # print 'init assignment'
    #         self.initial_furthest = furthest
    #         self.initial_furthest_coord = each


# Useful functions for both offence and defence
# Sets self.team to red or blue.
  def set_team(self):
      if self.index % 2 == 0 or not self.index:
          self.team = 'Red'
      else:
          self.team = 'Blue'

    # Returns defended food for defender, and target food for offensive agent.
  def get_target_food_coords(self, gameState, other_side = 0):
      curr_pos = gameState.getAgentPosition(self.index)
      target_food = []

    #   Boolean is true if offensive agent returning to base.
      if self.index < 2 or other_side:
          for i, j in enumerate(CaptureAgent.getFoodYouAreDefending(self, gameState)):
              for k, l in enumerate(j):
                  if l:
                      target_food.append((i, k))

      else:
          for i, j in enumerate(CaptureAgent.getFood(self, gameState)):
              for k, l in enumerate(j):
                  if l:
                      target_food.append((i, k))

      return target_food

  def can_see_foe(self, gameState):
      invaders = []
      if self.index < 2:
          for indices in self.getOpponents(gameState):
              self.features['invaders'] = 0
              if gameState.getAgentPosition(indices):
                  invaders.append(gameState.getAgentPosition(indices))
                  self.features['invaders'] += 1
      else:
          for indices in self.getOpponents(gameState):
              if gameState.getAgentPosition(indices):
                  invaders.append(gameState.getAgentPosition(indices))
                #   print "Offensive agent sees foe."
                #   Check if for is a pacman, or a ghost.
                  self.features['defender'] = 1

      return invaders

# Determines if current state in successor is an offensive or defensive state to be in.
  def defended_position(self, successor):
      if self.index < 2:
          self.features['on_defense'] = 1
          if successor.isRed(successor.getAgentPosition(self.index)):
              if self.team == 'Blue':
                  self.features['on_defense'] = 0
      else:
          self.features['on_offense'] = 0
          if successor.isRed(successor.getAgentPosition(self.index)):
              if self.team == 'Blue':
                  self.features['on_offense'] = 1

# Checks if successor state is same as previous state.
# TODO: Update this to work as desired, not detect if action is stop.
  def just_there(self, gameState, successor):
      nxt_agent_pos = successor.getAgentPosition(self.index)
      if nxt_agent_pos == gameState.getAgentPosition(self.index):
          self.features['is_prev_state'] = 1

  def chooseAction(self, gameState):
    """
    Gamestate Deets
        Odd team number -> Blue team
        Even team number -> Red team

        Grid is 0-30 x 0-30 in (x, y) form. Not (r, c).

    """

    # self.debugDraw((15,0), [1,0,0], clear=False)

    # print 'for recording.'

    self.action_val = util.Counter()
    self.features = util.Counter()
    # return self.defender(gameState)

    # self.food_deposited_count += 1

    # print 'self.food_deposited_count', self.food_deposited_count

    if self.index < 2:
        start = time.time()
        best_action = self.defender(gameState)
        print 'eval time for defender: %.4f' % (time.time() - start)
        return best_action[0]

    else:
        start = time.time()
        best_action = self.offensive_agent(gameState)
        print 'eval time for offensive agent: %.4f' % (time.time() - start)
        return best_action[0]



  def offensive_agent(self, gameState):
      # Current position as coordinate
      curr_pos = gameState.getAgentPosition(self.index)

      food_coords = self.get_target_food_coords(gameState)

  #   Sets held_food weighting, returns true if back_to_base set
      if self.check_held_food(gameState):
        #   if self.team == "Blue":
          target_food_coords = self.get_target_food_coords(gameState, 1)
          closest = float('inf')
          for each in target_food_coords:
              if self.distancer.getDistance(curr_pos, each) < closest:
                  closest_coord = each
                  closest_food_dist = self.distancer.getDistance(curr_pos, each)
      else:
          closest_food_dist = float('inf')
          for each in food_coords:
              if self.distancer.getDistance(curr_pos, each) < closest_food_dist:
                  closest_coord = each
                  closest_food_dist = self.distancer.getDistance(curr_pos, each)

    #   Check for nearby enemy defender prior to checking actions.
      seen_enemy_coord = self.can_see_foe(gameState)
      if seen_enemy_coord:
          #   Coordinate of cloest foe.
          closest_foe = self.get_closest_foe(gameState, seen_enemy_coord)
          og_dist = self.distancer.getDistance(curr_pos, closest_foe)

      for action in gameState.getLegalActions(self.index):
          successor = gameState.generateSuccessor(self.index, action)

      #   Did we score a point? If so, increment dropped food counter
          if self.team == "Blue":
              if self.getScore(gameState) > self.getScore(successor):
                  self.food_deposited_count += 1
          elif self.team == "Red":
              if self.getScore(gameState) < self.getScore(successor):
                  self.food_deposited_count += 1

          new_pos = successor.getAgentPosition(self.index)

          if len(self.get_target_food_coords(gameState)):
              new_dist = self.distancer.getDistance(new_pos, closest_coord)
              self.features['closest_food'] = closest_food_dist - new_dist

          if seen_enemy_coord:
              new_dist = self.distancer.getDistance(new_pos, closest_foe)
              diff = og_dist - new_dist

            #   if new_dist <= 3:
              if self.team == "Blue":
                  if gameState.isRed(closest_foe):
                      self.features['defender'] = diff

              if self.team == "Red":
                  if not gameState.isRed(closest_foe):
                      self.features['defender'] = diff

            # Check if nearby ghost.

        #   Check if next state would deposit food.
            # current score - sucessor_score.


        #   Check if in a offensive position
          self.defended_position(successor)

        #   Check if previous state.
          self.just_there(gameState, successor)

        #   if self.check_held_food(gameState):
          if self.index > 1:
              print '\n\nself.features', self.features
          for key in self.features:
              self.action_val[action] += self.features[key] * \
                                              self.get_offensive_weights()[key]

          self.features = util.Counter()

      best = -float('inf')
      for key in self.action_val:
          if self.action_val[key] > best:
              best = self.action_val[key]
              best_move = key

      if self.index > 1:
          print 'best_move', best_move, 'with val', best,'\n'
      return ([best_move, best])

  def check_held_food(self, gameState):
        if self.team == 'Blue':
            food_held = self.starting_target_food - len(self.get_target_food_coords(gameState))\
                                                  - self.food_deposited_count
            if self.getScore(gameState) - food_held < -1 or \
                                len(self.get_target_food_coords(gameState)) == 2:
                self.features['back_to_base'] = food_held
                return True

        elif self.team == 'Red':
            food_held = self.starting_target_food - len(self.get_target_food_coords(gameState))\
                                                  + self.food_deposited_count
            if self.getScore(gameState) + food_held > 1 or \
                                len(self.get_target_food_coords(gameState)) == 2:
                self.features['back_to_base'] = food_held
                return True

        return False

  def get_closest_foe(self, gameState, foe_list):
      closest = float('inf')

      for foe in foe_list:

          if self.distancer.getDistance(gameState.getAgentPosition(self.index), foe) < closest:
              closest = self.distancer.getDistance(gameState.getAgentPosition(self.index), foe)
              closer_coord = foe

      return closer_coord


  def get_offensive_weights(self):
      return {
                'on_offense' : 50,
                'is_prev_state' : -100,
                'closest_food' : 200,
                'back_to_base' : 50,
                'defender' : -200
                # To implement:
                # 'defender' : -500,
                # 'num_eaten_food' : 10,
                # 'coord_foe_dist' : 30,
                # 'defender_food_eaten' : 30,
            }


  def defender(self, gameState):

    # Current position as coordinate
    curr_pos = gameState.getAgentPosition(self.index)

    # Check if invader nearby
    seen_enemy_coord = self.can_see_foe(gameState)
    # if len(seen_enemy_coords) > 1:
    #     closest_foe = self.get_closest_foe()

    if seen_enemy_coord:
        dist_to_foe = self.distancer.getDistance(curr_pos, seen_enemy_coord[0])

    # Find furthest food to protect. Perhaps replace with closest food to border..
    target_food_coords = self.get_target_food_coords(gameState)
    furthest = -float('inf')
    for each in target_food_coords:
        if self.distancer.getDistance(curr_pos, each) > furthest:
            furthest_coord = each
            furthest = self.distancer.getDistance(curr_pos, each)

    if self.missing_food(gameState) > 0:
        coord = self.get_missing_food(gameState)
        coord = list(coord)[0]
        missing_food_dist = self.distancer.getDistance(curr_pos, coord)

    for action in gameState.getLegalActions(self.index):
        successor = gameState.generateSuccessor(self.index, action)
        # seen_enemy_coord = self.can_see_foe(successor)

        new_pos = successor.getAgentPosition(self.index)

        # New distance to furthest food.
        new_dist = self.distancer.getDistance(new_pos, furthest_coord)
        self.features['furthest_food'] = furthest - new_dist


        ## For static point to defend from, use initialize code an below.
        # new_dist = self.distancer.getDistance(new_pos, self.initial_furthest_coord)
        # self.features['furthest_food'] = self.initial_furthest - new_dist

        # Check if now closer to invader
        if seen_enemy_coord:
            new_dist = self.distancer.getDistance(new_pos, seen_enemy_coord[0])
            self.features['invaders'] = dist_to_foe - new_dist

        # print 'num curr food..', len(self.get_target_food_coords(gameState))

        # if a piece of food disappeared, check which action gets us closer
        if self.missing_food(gameState) > 0:
            coord = self.get_missing_food(gameState)
            coord = list(coord)[0]

            new_missing_food_dist = self.distancer.getDistance(new_pos, coord)
            self.features['vanished_food'] = missing_food_dist - new_missing_food_dist
            # return self.get_action_to_coord(coord, gameState)

        # Check if next state is defended side
        self.defended_position(successor)

        # Check if this action would result in prev state
        self.just_there(gameState, successor)

        # Check amount of defended food currently picked up by foe
        # self.features['num_eaten_food'] = len(target_food_coords) - \
        #                                         gameState.getScore()
        # if self.features['num_eaten_food'] < 0:
        #     self.features['num_eaten_food'] = 0

        # print self.features
        for key in self.features:
            self.action_val[action] += self.features[key] * \
                                            self.get_defender_weights()[key]


    # Of possible actions, return the best.
    best = -float('inf')
    for key in self.action_val:
        if self.action_val[key] > best:
            best = self.action_val[key]
            best_move = key

    return ([best_move, best])

  def get_missing_food(self, gameState):
      prev_game_state = self.getPreviousObservation()
      coord = set(self.get_target_food_coords(prev_game_state)) - \
                      set(self.get_target_food_coords(gameState))

    #   self.missing = coord
      return coord

  def missing_food(self, gameState):
      prev_game_state = self.getPreviousObservation()
      prev_food = 0
      curr_food = 0
      if prev_game_state is not None:
          prev_food = len(self.get_target_food_coords(prev_game_state))
          curr_food = len(self.get_target_food_coords(gameState))

      return prev_food - curr_food


# For multiple coordinate results.
  # def get_closest_foe(self, gameState):


  def get_defender_weights(self):
      return {
                'on_defense' : 100,
                'is_prev_state' : -100,
                'furthest_food' : 15,
                'vanished_food' : 800,
                'invaders' : 1000,
                # To implement:
                'num_eaten_food' : 10,
                'coord_foe_dist' : 30,
                'defender_food_eaten' : 30,
            }
