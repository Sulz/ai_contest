# myTeam.py
# ---------
# Licensing Information:  You are free to use or extend these projects for
# educational purposes provided that (1) you do not distribute or publish
# solutions, (2) you retain this notice, and (3) you provide clear
# attribution to UC Berkeley, including a link to http://ai.berkeley.edu.
#
# Attribution Information: The Pacman AI projects were developed at UC Berkeley.
# The core projects and autograders were primarily created by John DeNero
# (denero@cs.berkeley.edu) and Dan Klein (klein@cs.berkeley.edu).
# Student side autograding was added by Brad Miller, Nick Hay, and
# Pieter Abbeel (pabbeel@cs.berkeley.edu).


from captureAgents import CaptureAgent
import random, time, util
from game import Directions
import game

#################
# Team creation #
#################

def createTeam(firstIndex, secondIndex, isRed,
               first = 'FeatureAgent', second = 'FeatureAgent'):
  """
  This function should return a list of two agents that will form the
  team, initialized using firstIndex and secondIndex as their agent
  index numbers.  isRed is True if the red team is being created, and
  will be False if the blue team is being created.

  As a potentially helpful development aid, this function can take
  additional string-valued keyword arguments ("first" and "second" are
  such arguments in the case of this function), which will come from
  the --redOpts and --blueOpts command-line arguments to capture.py.
  For the nightly contest, however, your team will be created without
  any extra arguments, so you should make sure that the default
  behavior is what you want for the nightly contest.
  """

  # The following line is an example only; feel free to change it.
  return [eval(first)(firstIndex), eval(second)(secondIndex)]

##########
# Agents #
##########

class FeatureAgent(CaptureAgent):
  """
  A Dummy agent to serve as an example of the necessary agent structure.
  You should look at baselineTeam.py for more details about how to
  create an agent as this is the bare minimum.
  """
  def registerInitialState(self, gameState):
    """
    This method handles the initial setup of the
    agent to populate useful fields (such as what team
    we're on).

    A distanceCalculator instance caches the maze distances
    between each pair of positions, so your agents can use:
    self.distancer.getDistance(p1, p2)

    IMPORTANT: This method may run for at most 15 seconds.
    """

    '''
    Make sure you do not delete the following line. If you would like to
    use Manhattan distances instead of maze distances in order to save
    on initialization time, please take a look at
    CaptureAgent.registerInitialState in captureAgents.py.
    '''
    CaptureAgent.registerInitialState(self, gameState)

    '''
    Your initialization code goes here, if you need any.
    '''

    # This is purely for the defensive agent to know the location of the closest defended food to the border
    def get_target_food_coords2(self, gameState):
        curr_pos = gameState.getAgentPosition(self.index)
        target_food = []
        if self.index < 2:
            for i, j in enumerate(self.getFoodYouAreDefending(gameState)):
                for k, l in enumerate(j):
                    if l:
                        target_food.append((i, k))
        else:
            for i, j in enumerate(self.getFood(gameState)):
                for k, l in enumerate(j):
                    if l:
                        target_food.append((i, k))
        return target_food

    def ref_base_food(self):
        furthest = -float('inf')
        for each in self.getFoodYouAreDefending(gameState).asList():
            if self.distancer.getDistance(gameState.getAgentPosition(self.index), \
                                                each) > furthest:
                furthest = self.distancer.getDistance(gameState.getAgentPosition(self.index), \
                                                        each)
                target = each
        return target


    self.starting_target_food = len(get_target_food_coords2(self, gameState))
    self.return_food = ref_base_food(self)
    # print 'self.return_food', self.return_food

    self.food_deposited_count = 0
    self.foe_points_scored = 0

    self.set_team()
    self.features = util.Counter()

    # Sit near border
    # target_food_coords = self.get_target_food_coords(gameState)
    # furthest = -float('inf')
    # for each in target_food_coords:
    #     if self.distancer.getDistance(gameState.getAgentPosition(self.index), each) > furthest:
    #         # furthest_coord = each
    #         furthest = self.distancer.getDistance(gameState.getAgentPosition(self.index), each)
    #         # print 'init assignment'
    #         self.initial_furthest = furthest
    #         self.initial_furthest_coord = each


# Useful functions for both offence and defence
# Sets self.team to red or blue.
  def set_team(self):
      if self.index % 2 == 0 or not self.index:
          self.team = 'Red'
      else:
          self.team = 'Blue'

    # Returns defended food for defender, and target food for offensive agent.
  def get_target_food_coords(self, gameState, other_side = 0):
      curr_pos = gameState.getAgentPosition(self.index)
      target_food = []

    #   Boolean is true if offensive agent returning to base.
      if self.index < 2 or other_side:
          for i, j in enumerate(CaptureAgent.getFoodYouAreDefending(self, gameState)):
              for k, l in enumerate(j):
                  if l:
                      target_food.append((i, k))

      else:
          for i, j in enumerate(CaptureAgent.getFood(self, gameState)):
              for k, l in enumerate(j):
                  if l:
                      target_food.append((i, k))

      return target_food

  def can_see_foe(self, gameState):
      invaders = []
      if self.index < 2:
          for indices in self.getOpponents(gameState):
              self.features['invaders'] = 0
              if gameState.getAgentPosition(indices):
                  invaders.append(gameState.getAgentPosition(indices))
                  self.features['invaders'] += 1
      else:
          for indices in self.getOpponents(gameState):
              if gameState.getAgentPosition(indices):
                  if (gameState.isRed(gameState.getAgentPosition(indices)) and self.team == "Blue") \
                        or (not gameState.isRed(gameState.getAgentPosition(indices)) \
                            and self.team == "red"):
                      invaders.append(gameState.getAgentPosition(indices))
            #   elif !gameState.isRed(getAgentPosition(indices)) and self.team == "Red":
                #   if gameState.isRed(gameState.getAgentPosition(indices)) and\
                #   print "Offensive agent sees foe."
                #   Check if for is a pacman, or a ghost.

                  self.features['defender'] = indices

      return invaders

# Determines if current state in successor is an offensive or defensive state to be in.
  def defended_position(self, successor):
      if self.index < 2:
          if successor.isRed(successor.getAgentPosition(self.index)):
              if self.team == 'Blue':
                  self.features['on_defense'] = 0
              if self.team == "Red":
                  self.features['on_defense'] = 5
          elif not successor.isRed(successor.getAgentPosition(self.index)):
              if self.team == "Red":
                  self.features['on_defense'] = -5
      else:
          self.features['on_offense'] = 1
          if successor.isRed(successor.getAgentPosition(self.index)):
              if self.team == 'Red':
                  self.features['on_offense'] = 0

# Checks if successor state is same as previous state.
# TODO: Update this to work as desired, not detect if action is stop.
  def just_there(self, gameState, successor):
      nxt_agent_pos = successor.getAgentPosition(self.index)
      if nxt_agent_pos == gameState.getAgentPosition(self.index):
          self.features['is_prev_state'] = 1

  def chooseAction(self, gameState):
    """
    Gamestate Deets
        Odd team number -> Blue team
        Even team number -> Red team

        Grid is 0-30 x 0-30 in (x, y) form. Not (r, c).

    """

    # print dir(gameState)

    # self.debugDraw([(5,1), (5, 2)], [0,0,1], clear=False)

    # print 'for recording.'

    self.action_val = util.Counter()
    self.features = util.Counter()

    # return random.choice(gameState.getLegalActions(self.index))

    # print 'self.food_deposited_count', self.food_deposited_count

    if self.index < 2:
        # start = time.time()
        best_action = self.defender(gameState)
        # print 'eval time for defender: %.4f' % (time.time() - start)
        return best_action

    else:
        # start = time.time()
        best_action = self.offensive_agent(gameState)
        # print 'eval time for offensive agent: %.4f' % (time.time() - start)
        return best_action


  def opposite_action(self, action):
      if action == "North":
          return "South"
      elif action == "East":
          return "West"
      elif action == "South":
          return "North"
      elif action == "West":
          return "East"


  def offensive_agent(self, gameState):
      # Current position as coordinate
      self.features = util.Counter()
      curr_pos = gameState.getAgentPosition(self.index)

      food_coords = self.get_target_food_coords(gameState)

   #   Did we score a point? If so, increment dropped food counter
      previous_state = self.getPreviousObservation()
      if previous_state:
          if self.getScore(gameState) > self.getScore(previous_state):
              if self.team == "Red":
                  self.food_deposited_count += abs(self.getScore(gameState) - self.getScore(previous_state))
              else:
                  self.foe_points_scored += abs(self.getScore(gameState) - self.getScore(previous_state))

          elif self.getScore(gameState) < self.getScore(previous_state):
              if self.team == "Blue":
                  self.food_deposited_count += abs(self.getScore(gameState) - self.getScore(previous_state))
              else:
                  self.foe_points_scored += abs(self.getScore(gameState) - self.getScore(previous_state))

    #   print 'self.foe_points_scored', self.foe_points_scored
    #   print 'self.food_deposited_count', self.food_deposited_count

      og_closest_food_dist = float('inf')
      for each in food_coords:
         if self.distancer.getDistance(curr_pos, each) < og_closest_food_dist:
             og_closest_coord = each
             og_closest_food_dist = self.distancer.getDistance(curr_pos, each)
             self.debugDraw(og_closest_coord, [1,0,0], clear=True)


      self.debugDraw(self.return_food, [0,1,0])
    #   print "\n\nnext"
      for action in gameState.getLegalActions(self.index):
        #   print '\n action', action
          self.features = util.Counter()
          """Check new distance to closest friendly food, weigh dist with foodheld

          Check new distance to closest foe food, weight dist with value of new food.

          Somethi in the way. Do another search bfs for next nearest food"""

          """Is there a ghost there? (Do we respawn?)"""
        #   print 'self.check_held_food(gameState)', self.check_held_food(gameState)
          self.features['back_to_base'] = self.check_held_food(gameState)
          successor = gameState.generateSuccessor(self.index, action)
          new_pos = successor.getAgentPosition(self.index)

        #   if gameState.isRed(new_pos) and self.team == "Blue":
        #       self.features['on_offense'] = 1
        #   elif not gameState.isRed(new_pos) and self.team == "Red":
        #       self.features['on_offense'] = 1

          if food_coords:
              closest_food_dist = self.distancer.getDistance(new_pos, og_closest_coord)
          if not closest_food_dist:
              self.features['closest_food'] = 10
          else:
              self.features['closest_food'] = float(1)/float(closest_food_dist)

          if self.distancer.getDistance(new_pos, self.return_food):
              self.features['back_to_base'] = 5 * float(self.features['back_to_base'])/ \
                                                float(self.distancer.getDistance(new_pos, self.return_food))
          if len(self.getFood(gameState).asList()) == 2:
              self.features['back_to_base'] = 1000/float(self.distancer.getDistance(new_pos, self.return_food))

          for indices in self.getOpponents(gameState):
            #   print 'gameState.getAgentPosition(indices)', gameState.getAgentPosition(indices)
              if gameState.getAgentPosition(indices):
                  if self.distancer.getDistance(new_pos, gameState.getAgentPosition(indices)) < 3:
                      if gameState.isRed(successor.getAgentPosition(indices)) and \
                                self.team == "Blue" and self.distancer.getDistance(new_pos, gameState.getAgentPosition(indices))\
                                and gameState.getAgentState(indices).scaredTimer < 2:
                            # print '\n\ngameState.getAgentState(indices)', gameState.getAgentState(indices), "\n\n"
                            # for each in gameState.getAgentState(indices):
                            #     print 'each', each
                            self.features['ghost'] = float(1)/ \
                                            float(self.distancer.getDistance(new_pos, gameState.getAgentPosition(indices)))

                      elif not gameState.isRed(successor.getAgentPosition(indices)) and \
                                self.team == "Red" and self.distancer.getDistance(new_pos, gameState.getAgentPosition(indices))\
                                and gameState.getAgentState(indices).scaredTimer < 2:
                                self.features['ghost'] = float(1)/ \
                                        float(self.distancer.getDistance(new_pos, gameState.getAgentPosition(indices)))
                            #   self.features['ghost'] = 1


        #   self.action_val = util.Counter()
        #   print 'self.features', self.features
          for key in self.features:
              self.action_val[action] += self.features[key] * \
                                            self.get_offensive_weights()[key]
              if action == "Stop":
                  self.action_val[action] -= .5*self.action_val[action]
            #   print 'action', action, 'has val:', self.action_val[action]


      best = -float('inf')
    #   print '\nself.action_val', self.action_val
      for key in self.action_val:
          if self.action_val[key] > best:
              best = self.action_val[key]
              best_move = key

    #   print "\ntaking action:", best_move
      return best_move



  def check_held_food(self, gameState):

        if self.team == 'Blue':
            food_held = self.starting_target_food - len(self.get_target_food_coords(gameState))\
                                                  - self.food_deposited_count + gameState.getScore()
            self.features['back_to_base'] = food_held
            # print '\nheld food:', food_held
            return food_held

        elif self.team == 'Red':
            food_held = self.starting_target_food - len(self.get_target_food_coords(gameState))\
                                                  - self.food_deposited_count

            self.features['back_to_base'] = food_held
            # print '\nheld food:', food_held
            return food_held

        # self.features['back_to_base'] = 0
        # return False

  def get_closest_foe(self, gameState, foe_list):
      closest = float('inf')

      for foe in foe_list:

          if self.distancer.getDistance(gameState.getAgentPosition(self.index), foe) < closest:
              closest = self.distancer.getDistance(gameState.getAgentPosition(self.index), foe)
              closer_coord = foe

      return closer_coord


  def get_offensive_weights(self):
      return {
                'on_offense' : 100,
                'is_prev_state' : -100,
                'closest_food' : 200,
                'back_to_base' : 200,
                'ghost' : -200
                # To implement:
                # 'defender' : -500,
                # 'num_eaten_food' : 10,
                # 'coord_foe_dist' : 30,
                # 'defender_food_eaten' : 30,
            }


  def defender(self, gameState):
    # Current position as coordinate
    curr_pos = gameState.getAgentPosition(self.index)

    # Check if invader nearby
    seen_enemy_coord = self.can_see_foe(gameState)
    # if len(seen_enemy_coords) > 1:
    #     closest_foe = self.get_closest_foe()

    if seen_enemy_coord:
        dist_to_foe = self.distancer.getDistance(curr_pos, seen_enemy_coord[0])

    # Find furthest food to protect. Perhaps replace with closest food to border..
    target_food_coords = self.get_target_food_coords(gameState)
    furthest = -float('inf')
    for each in target_food_coords:
        if self.distancer.getDistance(curr_pos, each) > furthest:
            furthest_coord = each
            furthest = self.distancer.getDistance(curr_pos, each)
        # else:
          # furthest_coord = 0

    if self.missing_food(gameState) > 0:
        coord = self.get_missing_food(gameState)
        coord = list(coord)[0]
        missing_food_dist = self.distancer.getDistance(curr_pos, coord)

    # if self.team == "Red":
    #     print "\n\nNext\n"
    for action in gameState.getLegalActions(self.index):
        successor = gameState.generateSuccessor(self.index, action)
        # seen_enemy_coord = self.can_see_foe(successor)

        new_pos = successor.getAgentPosition(self.index)

        # New distance to furthest food.
        if furthest != -float('inf'):
          new_dist = self.distancer.getDistance(new_pos, furthest_coord)
          self.features['furthest_food'] = furthest - new_dist


        ## For static point to defend from, use initialize code an below.
        # new_dist = self.distancer.getDistance(new_pos, self.initial_furthest_coord)
        # self.features['furthest_food'] = self.initial_furthest - new_dist

        # Check if now closer to invader
        if seen_enemy_coord:
            new_dist = self.distancer.getDistance(new_pos, seen_enemy_coord[0])
            """Ensure we are not scared"""
            if gameState.getAgentState(self.index).scaredTimer > 1:
                if new_dist < 2:
                    self.features['invaders'] = -2
            else:
                self.features['invaders'] = dist_to_foe - new_dist

        # print 'num curr food..', len(self.get_target_food_coords(gameState))

        # if a piece of food disappeared, check which action gets us closer
        if self.missing_food(gameState) > 0:
            coord = self.get_missing_food(gameState)
            coord = list(coord)[0]

            new_missing_food_dist = self.distancer.getDistance(new_pos, coord)
            self.features['vanished_food'] = missing_food_dist - new_missing_food_dist
            # return self.get_action_to_coord(coord, gameState)

        # Check if next state is defended side
        self.defended_position(successor)

        # Check if this action would result in prev state
        self.just_there(gameState, successor)

        # Check amount of defended food currently picked up by foe
        # self.features['num_eaten_food'] = len(target_food_coords) - \
        #                                         gameState.getScore()
        # if self.features['num_eaten_food'] < 0:
        #     self.features['num_eaten_food'] = 0
        # if self.team == "Red":
            # print 'action', action
            # print self.features
        for key in self.features:
            self.action_val[action] += self.features[key] * \
                                            self.get_defender_weights()[key]


    # Of possible actions, return the best.
    best = -float('inf')
    for key in self.action_val:
        if self.action_val[key] > best:
            best = self.action_val[key]
            best_move = key

    # if self.team == "Red":
    #     print self.features
    return best_move

  def get_missing_food(self, gameState):
      prev_game_state = self.getPreviousObservation()
      coord = set(self.get_target_food_coords(prev_game_state)) - \
                      set(self.get_target_food_coords(gameState))

    #   self.missing = coord
      return coord

  def missing_food(self, gameState):
      prev_game_state = self.getPreviousObservation()
      prev_food = 0
      curr_food = 0
      if prev_game_state is not None:
          prev_food = len(self.get_target_food_coords(prev_game_state))
          curr_food = len(self.get_target_food_coords(gameState))

      return prev_food - curr_food


# For multiple coordinate results.
  # def get_closest_foe(self, gameState):


  def get_defender_weights(self):
      return {
                'on_defense' : 10000,
                'is_prev_state' : -100,
                'furthest_food' : 15,
                'vanished_food' : 800,
                'invaders' : 1000,
                # To implement:
                'coord_foe_dist' : 30,
                'defender_food_eaten' : 30,
            }
