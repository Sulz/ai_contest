This README outlines the behavior of the agents, as developed by the team 
Bugs_R_Features, and the reasoning behind them.

Both the defensive and offensive agents make judgments on which action is 
the best action to take based on a set of observations.
Aspects of these observations (such as distance) are taken into account and
computed into a decision. The distancer function is used for determining 
these distances.

While the code in both agents does contain 'if' statements. These are to 
prevent runtime errors such as divide by 0. Allowing a method of recognizing when
the successor state contains a given features (ie, distance to dot from a state
that has a dot is 0).

An overview of the behavior of both agents is as follows:

The Defender:
When the index is < 2, the agent is a defender.

The defender has two primary goals:
 - Defend the food
 - Chase invading ghosts.

To defend the food (while there are no visible foes), the agent determines
which piece of food is the furthest away, then starts moving toward it. 
This results in the moving back and forward generally somewhere around the 
middle of its territory.

The defender is an aggresive defender. When either the defender or 
offensive agents have spotted a foe, actions toward that foe gain a large 
bonus. This ensures the defender will chase the foe to extinction.
A feature of this is the defender will start moving toward ghosts on the
other side of the map, leading him to patrol towards the border.

While the defender is aggresive, it's not reckless. While there may be a 
visible ghost in the other teams' territory, the defender always ensures 
it never leaves its own territory (done through checking isRed() and 
losing a positive weight when Red is non-defended territory.).

The defensive agent also notices when a piece of food vanishes. When it
does, there is a positive weighting for actions toward the missing food (as
an enemy must be nearby!).

To avoid being eaten while the defender is scared, the defending agent endeavors
to shadow the scary invader by 2 spaces.

Further Development
Instead of having the defensive agent patrol between the two furthest
pieces of food from their midpoint. The agent could instead initialize with 
a list of food tactically situated nearby the border such to best notice
an invader invading.


The Offensive Agent
When agent index is > 2, the agent is the offensive agent.

The offensive agent was found to be more complex that the defensive agent.
His priotities are based on the following, which comprise the factors 
leading to the final action decision:
  - Gather food
  - Deposit food
  - Avoid ghosts
  - Consider going for the pellet
  - Eat ghosts when they're scared

To gather food, the agent determines the closest piece of edible food. The 
value of moving toward this food is based on the distance to the food. Thus 
when the food is further away, the value of pursuing it drops.

To deposit gathered food, the agent considers how much food is currently 
held. This is then evaluated against the distance required to deposit the 
food. Thus, when there is a piece of food en-route to depositing currently 
held food, it is considered beneficial to pick it up.

By evaluating the distance required for both gathering food and depositing 
food, the agent is able to decide whether or not its more valuable to 
deposit the food vs gather more.

To avoid ghosts, the offensive agent again considers the distance to an 
observed foe. Actions towards the foe are only negatively weighted while the
foe is within 3 tiles of the agent. This is to prevent the agent from 
fearing ghosts over the border, which prevent the pursuing of target 
food.

The value of the pellet is fairly small. It's implemented with the intention that
the agent will collect the power pellet en-route to his next objective (whether 
its base or more food)

When the ghosts are scared, instead of actions toward the ghost being 
negatively weighted. They are the positive equivalent of the negative value.
Furthermore, if nearby ghosts are scared, this emboldens the agent to 
pursue more food by reducing the weight of the 'back_to_base' weight.

Further Development
In circumstances where the offensive agent is stuck in a stalemate against a
defending enemy (ie, they both chase, retreat, chase retreat ...), the
agent could perform a search such as bfs, aStar, or even minimax to determine 
another path that circumvents the nearby defending foe. It's unfortunate 
such functionality wasn't implemented.

Both agents could also possibly benefit from some lightly weighted noisy
distance considerations.